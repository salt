{%- set roleproxy_pillar = pillar['salt']['roleproxy'] -%}

salt_roleproxy_packages:
  pkg.installed:
    - names:
      - salt-netbox-roleproxy
    - watch_in:
      - service: salt_roleproxy_service

salt_roleproxy_sysconfig:
  file.keyvalue:
    - name: /etc/sysconfig/roleproxy
    - separator: '='
    - show_changes: False
    - key_values:
        NB_HOST: {{ roleproxy_pillar['nb_host'] }}
        NB_TOKEN: {{ roleproxy_pillar['nb_token'] }}
    - require:
      - pkg: salt_roleproxy_packages
    - watch_in:
      - service: salt_roleproxy_service

salt_roleproxy_service_enable:
  service.enabled:
    - name: roleproxy
    - require:
      - pkg: salt_roleproxy_packages

salt_roleproxy_service:
  service.running:
    - name: roleproxy
    - watch:
      - pkg: salt_roleproxy_packages
      - file: salt_roleproxy_sysconfig
