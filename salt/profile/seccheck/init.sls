seccheck_packages:
  pkg.installed:
    - pkgs:
      - seccheck

seccheck_files:
  file.managed:
    - user: root
    - mode: '0644'
    - template: jinja
    - names:
      - /etc/sysconfig/seccheck:
        - source: salt:///{{ slspath }}/files/etc/sysconfig/seccheck
      - /etc/security/autologout.conf:
        - source: salt:///{{ slspath }}/files/etc/security/autologout.conf

seccheck_service:
  service.running:
    - name: seccheck-autologout.timer
    - enable: True
