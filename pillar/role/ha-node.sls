firewalld:
  zones:
    internal:
      protocols:
        - vrrp
    backend:
      protocols:
        - udp
