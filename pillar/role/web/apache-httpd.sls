{%- set host = grains['host'] -%}
{%- set fqdn = grains['fqdn'] -%}

apache:
  global:
    ServerAdmin: system@lysergic.dev

profile:
  apache-httpd:
   snippets:
    ssl_{{ host }}:
      - 'SSLCertificateFile    "/etc/ssl/{{ host }}/{{ fqdn }}.crt"'
      - 'SSLCertificateKeyFile "/etc/ssl/{{ host }}/{{ fqdn }}.key"'
