{%- set aapillar = salt['pillar.get']('profile:apparmor') %}

{%- if 'local' in aapillar %}
{%- for profile, lines in aapillar['local'].items() %}
/etc/apparmor.d/local/{{ profile }}:
  file.managed:
    - contents: {{ lines }}
    - watch_in:
      - module: apparmor_reload
{%- endfor %}

{%- if aapillar['local'] | length %}
apparmor_reload:
  module.run:
    - name: service.reload
    - m_name: apparmor
    - onchanges:
      {%- for profile in aapillar['local'] %}
      - file: /etc/apparmor.d/local/{{ profile }}
      {%- endfor %}
{%- endif %}
{%- endif %}
