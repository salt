{%- from slspath ~ '/../map.jinja' import listeners -%}
{#-
{%- from '/tmp/salt-libertacasa/pillar/global/macros.jinja' import listeners with context -%}
#}
{%- set host = grains['host'] -%}

sshd_config:
  ConfigBanner: |
    ### This file is managed via https://git.com.de/LibertaCasa/salt
    ### Manual changes will be overwritten
  ListenAddress: {{ listeners() | indent }}
  Protocol: 2
  SyslogFacility: AUTH
  LogLevel: FATAL
  HostKey:
    - /etc/ssh/{{ host }}
  HostKeyAlgorithms: ssh-ed25519-cert-v01@openssh.com
  HostCertificate: /etc/ssh/{{ host }}-cert.pub
  TrustedUserCAKeys: /etc/ssh/user_ca
  PasswordAuthentication: 'no'
  LoginGraceTime: 1m
  PermitRootLogin: 'no'
  StrictModes: 'yes'
  MaxAuthTries: 1
  MaxSessions: 3
  UsePAM: 'yes'
  X11Forwarding: 'no'
  PrintMotd: 'yes'
  PrintLastLog: 'yes'
  Subsystem: sftp /usr/lib/ssh/sftp-server
  Banner: /etc/ssh/banner

firewalld:
  zones:
    internal:
      services:
        - ssh
