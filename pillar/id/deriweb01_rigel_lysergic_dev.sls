{%- from 'map.jinja' import nginx_crtkeypair -%}
{%- set resolver = '[2a01:4f8:11e:2200::2]' -%}

{#- to-do: move other service hosts to backend dict #}
{%- set backend = {'takahe': 'http://takahe.rigel.lysergic.dev:8000'} %}

{#- generic macros #}
{%- macro proxyheaders(ForRemAdd=False) -%}
- proxy_set_header:
  - 'Host $host'
  - 'X-Real-IP $remote_addr'
  - 'X-Forwarded-For {{ '$remote_addr' if ForRemAdd else '$proxy_add_x_forwarded_for' }}'
  - 'X-Forwarded-Proto $scheme'
{%- endmacro -%}
{%- macro proxyheaders2(ForRemAdd=False) -%}
{{ proxyheaders(ForRemAdd) | indent(16) }}
{%- endmacro -%}
{%- macro jf_proxyheaders() -%}
{{ proxyheaders() | indent(16) }}
                  - 'X-Forwarded-Protocol $scheme'
                  - 'X-Forwarded-Host $http_host'
{%- endmacro -%}
{%- macro proxyheaders_upgrade() -%}
- proxy_set_header: Upgrade     $http_upgrade
- proxy_set_header: Connection  $connection_upgrade
{%- endmacro -%}
{%- macro proxyheaders2_upgrade() -%}
{{ proxyheaders_upgrade() | indent(16) }}
{%- endmacro -%}

{#- application specific macros #}
{%- macro lcwebirc(port, indentinner) -%}
            - location /webirc:
                - proxy_pass: https://[2a01:4f8:11e:2200::cafe]:{{ port }}
                - proxy_http_version:  1.1
                {{ proxyheaders_upgrade() | indent(16) }}
                - proxy_set_header:    X-Forwarded-For $remote_addr
                - proxy_set_header:    X-Forwarded-Proto $scheme
                - proxy_read_timeout:  600s
{%- endmacro -%}
{%- macro lcactivitypub(wkpath) -%}
            - location /.well-known/{{ wkpath }}:
                - proxy_pass: {{ backend.takahe }}
                - proxy_set_header: Host $http_host
                - resolver: '{{ resolver }} ipv4=off valid=24h'
{%- endmacro -%}
{%- macro matterbridge_media(domain, name, tls='load') -%}
          - server:
            - include:
              - snippets/listen
              - snippets/tls_{{ tls }}
              - snippets/tls
            - server_name: {{ domain }}
            - location /:
                - proxy_pass: http://{{ name }}.matterbridge.dericom02.rigel.lysergic.dev
{%- endmacro -%}

nginx:
  snippets:
    tls:
      - resolver: '{{ resolver }}'

    {#- certificate snippets #}
    {{ nginx_crtkeypair('exhaustedlife', 'exhausted.life') | indent }}
    {{ nginx_crtkeypair('georg', 'georg.systems') | indent }}
    {{ nginx_crtkeypair('libertacasa', 'liberta.casa') | indent }}
    {{ nginx_crtkeypair('libertacasa2', 'libertacasa.net') | indent }}
    {{ nginx_crtkeypair('load', 'load.casa') | indent }}
    {{ nginx_crtkeypair('lysergic', 'lysergic.dev') | indent }}
    {{ nginx_crtkeypair('lysergic_media', 'lysergic.media') | indent }}
    {{ nginx_crtkeypair('meet', 'meet.com.de') | indent }}
    {{ nginx_crtkeypair('takahe', 'social.liberta.casa') | indent }}
    {{ nginx_crtkeypair('pub_sectigo', 'pub') | indent }}
    {{ nginx_crtkeypair('up.chillnet.org', 'up.chillnet.org') | indent }}

    {#- locations shared between clearnet and Tor LibertaCasa servers #}
    libertacasa:
      - location /:
          - root: /srv/www/liberta.casa/static/website
          - index: index.html
      - location /register:
          - proxy_pass: http://[2a01:4f8:11e:2200::11]:8965
      - location /kiwi:
          - root: /srv/www/liberta.casa
          - index: index.html
          - try_files: $uri $uri/ =404
      - location /gamja:
          - root: /srv/www/liberta.casa
          - index: index.html

  servers:
    managed:
      general.conf:
        available_dir: /etc/nginx/conf.d
        config:
        - server_names_hash_bucket_size: 128
        - port_in_redirect: 'off'
        - server_tokens: 'off'
        - limit_conn_zone: $binary_remote_addr zone=georg_conn:15m
        - limit_req_zone: $binary_remote_addr zone=georg_req:15m rate=30r/s

      default-http.conf:
        config:
          - server:
            - server_name: localhost
            - listen:
              - '80'
            - location /:
                - root: /srv/www/htdocs
                - index: index.html

      cytube.conf:
        config:
          - server:
            - include:
              - snippets/listen
              - snippets/tls_lysergic
              - snippets/tls
            - server_name: party.lysergic.dev
            - location /:
                - proxy_pass: https://[2a01:4f8:11e:2200::11]:8250
                - proxy_set_header:
                  - X-Forwarded-For $proxy_add_x_forwarded_for
                  - X-Forwarded-Proto $scheme
                  - Host $http_host

      default-https.conf:
        config:
          - server:
            - listen:
              - '[2a01:4f8:11e:2200::dead]:443 ssl http2 default_server'
              - 10.0.10.7:443 ssl http2 default_server
            - include: snippets/tls_lysergic
            - root: /srv/www/htdocs

      georg.conf:
        config:
          {%- macro georg_includes() %}
              - include:
                - snippets/listen
                - snippets/tls_georg
                - snippets/tls
          {%- endmacro %}
          - server:
              {{ georg_includes() }}
              - server_name: www.georg-pfuetzenreuter.net www.georg-pfuetzenreuter.de www.georg-pfuetzenreuter.at 240600.xyz
              - return: 302 https://georg-pfuetzenreuter.net
          - server:
              {{ georg_includes() }}
              - server_name: gippy.at
              - return: 302 https://georg-pfuetzenreuter.net
          - server:
              {{ georg_includes() }}
                - snippets/robots
              - server_name: georg.systems georg-pfuetzenreuter.de
              - location /:
                  - proxy_pass: https://georg-public01.rigel.lysergic.dev:8989
                  {{ proxyheaders_upgrade() | indent(18) }}
                  - limit_conn: georg_conn 10
                  - limit_req: zone=georg_req burst=5 delay=3
                  - client_body_buffer_size: 2M
                  - resolver: '{{ resolver }} ipv6=on valid=60m'
          - server:
              {{ georg_includes() }}
              - server_name: georg-pfuetzenreuter.net pfuetzenreuter.at
              - location /:
                  - proxy_pass: https://georg-public01.rigel.lysergic.dev:8989
                  {{ proxyheaders_upgrade() | indent(18) }}
                  - limit_conn: georg_conn 10
                  - limit_req: zone=georg_req burst=10 delay=5
                  - client_body_buffer_size: 2M
                  - resolver: '{{ resolver }} ipv6=on valid=60m'
              - location /plain:
                  - root: /srv/www/georg
                  - autoindex: 'on'

      hedgedoc.conf:
        config:
          {%- macro lysergic_includes() %}
            - include:
              - snippets/listen
              - snippets/tls_lysergic
              - snippets/tls
          {%- endmacro %}
          - map $http_upgrade $connection_upgrade:
            - default: upgrade
            - "''": close
          - server:
            {{ lysergic_includes() }}
            - server_name: hd.lysergic.dev hedge.lysergic.dev
            - return: 302 https://hedgedoc.lysergic.dev
          - server:
            {{ lysergic_includes() }}
            - server_name: hedgedoc.lysergic.dev
            - location /:
                - proxy_pass: 'http://[2a01:4f8:11e:2200::11]:3000'
                {{ proxyheaders2() }}
            - location /socket.io/:
                - proxy_pass: 'http://[2a01:4f8:11e:2200::11]:3000'
                {{ proxyheaders2() }}
                {{ proxyheaders2_upgrade() }}
            - location /build:
                - root: /srv/www/hedgedoc

      hidden.conf:
        config:
          - server:
            - listen: '[2a01:4f8:11e:2200::dead]:8085'
            - server_name: qzzf2qcfbhievvs5nzkccuwddroipy62qjocqtmgcgh75vd6w57m7yad.onion
            {#- source the same LibertaCasa locations used on the clearnet #}
            - include: snippets/libertacasa
            {#- this proxies to an ergo tor=true + websocket=true listener #}
            {{ lcwebirc(6663) }}
            {#- we rewrite the Kiwi configuration with one using our onion websocket #}
            - location /kiwi/static/config.json:
                - root: /srv/www/liberta.casa
                - rewrite: ^/kiwi/static/config.json$ /kiwi/static/config_onion.json
            {#- we rewrite Gamja configuration with one using our onion websocket #}
            - location /gamja/config.json:
                - root: /srv/www/liberta.casa
                - rewrite: ^/gamja/config.json$ /gamja/config_onion.json
          - server:
            - listen: '[2a01:4f8:11e:2200::dead]:8085'
            - server_name: gitea.qzzf2qcfbhievvs5nzkccuwddroipy62qjocqtmgcgh75vd6w57m7yad.onion
            - location /:
                - proxy_pass: https://git.com.de
                - proxy_ssl_trusted_certificate: /etc/ssl/ca-bundle.pem
          - server:
            - listen: '[2a01:4f8:11e:2200::dead]:8085'
            - server_name: cgit.qzzf2qcfbhievvs5nzkccuwddroipy62qjocqtmgcgh75vd6w57m7yad.onion
            - location /:
                - proxy_pass: https://git.casa
                - proxy_ssl_trusted_certificate: /etc/ssl/ca-bundle.pem

      http.conf:
        config:
          - server:
            - listen:
              - 10.0.10.7:80 default_server
              - '[2a01:4f8:11e:2200::dead]:80 default_server'
            - include: snippets/robots
            - location /:
                - return: 301 https://$host$request_uri

      jellyfin.conf:
        config:
          - server:
            - include:
              - snippets/listen
              - snippets/tls_lysergic_media
              - snippets/tls
            - server_name: lysergic.media www.lysergic.media
            - set: '$jellyfin [2a01:4f8:11e:2200::abc]:8096'
            - client_max_body_size: 20M
            - add_header: X-Frame-Options         "SAMEORIGIN"
            - add_header: X-XSS-Protection        "1; mode=block"
            - add_header: X-Content-Type-Options  "nosniff"
            - location /:
                - proxy_pass: http://$jellyfin
                {{ jf_proxyheaders() }}
                - proxy_buffering: 'off'
            - location = /web/:
                - proxy_pass: http://$jellyfin/web/index.html
                {{ jf_proxyheaders() }}
            - location /socket:
                - proxy_pass: http://$jellyfin
                - proxy_http_version: 1.1
                {{ proxyheaders_upgrade() | indent(16) }}
                {{ jf_proxyheaders() }}
            - location @force_get:
                - proxy_method: GET
                - proxy_pass: http://$jellyfin
                {{ jf_proxyheaders() }}
                - proxy_buffering: 'off'
            - location /Items:
                - proxy_pass: http://$jellyfin
                {{ jf_proxyheaders() }}
                - proxy_buffering: 'off'
                - error_page: 550 = @force_get
                - if ($request_method = HEAD):
                  - return: 550
            - access_log: /var/log/nginx/jellyfin.access.log
            - error_log:  /var/log/nginx/jellyfin.error.log

      libertacasa.conf:
        config:
          {%- macro lc_includes() %}
            - include:
              - snippets/listen
              - snippets/tls
              - snippets/robots
          {%- endmacro %}
          - server:
            {{ lc_includes() }}
              - snippets/tls_libertacasa2
            - server_name: libertacasa.net libsh.net libsh.com libsso.net libsso.com
            - return: 302 https://liberta.casa
          - server:
            {{ lc_includes() }}
              - snippets/tls_libertacasa
              - snippets/error
              - snippets/libertacasa
            - server_name: liberta.casa lib.casa www.liberta.casa www.lib.casa
            - add_header: Onion-Location http://qzzf2qcfbhievvs5nzkccuwddroipy62qjocqtmgcgh75vd6w57m7yad.onion$request_uri
            {#- this proxies to an ergo websocket=true listener #}
            {{ lcwebirc(6661) }}
            {#- we still receive a lot of attempted requests to this, let's reject it properly #}
            - location /.well-known/matrix:
                - return: 410
            {#- IRC channel statistics/graphs #}
            - location /stats:
                - proxy_pass: https://stats.theia.psyched.dev/
            {#- ActivityPub for @liberta.casa domain #}
            {{ lcactivitypub('webfinger') }}
            {{ lcactivitypub('host-meta') }}
            {{ lcactivitypub('nodeinfo') }}

      matterbridge.conf:
        config:
          {{ matterbridge_media('load.casa', 'libertacasa-general') }}
          {{ matterbridge_media('irc.load.casa', 'libertacasa-irc') }}
          {{ matterbridge_media('up.chillnet.org', 'chillnet', 'up.chillnet.org') }}

      meet.conf:
        config:
          - server:
            {{ lysergic_includes() }}
            - server_name: meet.lysergic.dev meet.liberta.casa
            - location /:
                - proxy_pass: https://[2a01:4f8:11e:2200::c3]:5443/
                - proxy_http_version: 1.1
                {{ proxyheaders2(True) }}
                {{ proxyheaders2_upgrade() }}
                - tcp_nodelay: 'on'
            - modsecurity_rules: "'SecRuleRemoveById 949110'"

      openmeetings.conf:
        config:
          - server:
            - include:
              - snippets/listen
              - snippets/tls_meet
              - snippets/tls
            - server_name: meet.com.de www.meet.com.de
            - location /:
                - proxy_pass: https://[2a01:4f8:11e:2200::c3]:5443
                - proxy_buffering: 'off'
                - proxy_pass_header: Server
                {{ proxyheaders2(True) }}
            - location /openmeetings/wicket/websocket:
                - proxy_pass: https://[2a01:4f8:11e:2200::c3]:5443/openmeetings/wicket/websocket
                - proxy_buffering: 'off'
                - proxy_pass_header: Server
                {{ proxyheaders2(True) }}
                {{ proxyheaders2_upgrade() }}

      plantuml.conf:
        config:
          - server:
            {{ lysergic_includes() }}
            - server_name: pu.lysergic.dev uml.lysergic.dev
            - return: 302 https://plantuml.lysergic.dev
          - server:
            {{ lysergic_includes() }}
            - server_name: plantuml.lysergic.dev
            - location /:
                - proxy_pass: http://[2a01:4f8:11e:2200::11]:8086
                {#- PlantUML needs a very specific proxy configuration, hence not using the macros #}
                - proxy_set_header: HOST $host
                - proxy_set_header: X-Forwarded-Host $host
                - proxy_set_header: X-Forwarded-Proto $scheme
                - proxy_redirect: http://$host/ https://$host/

      pub.conf:
        config:
          - server:
            - include:
              - snippets/listen
              - snippets/tls_pub_sectigo
              - snippets/tls
            - server_name: pub.syscid.com
            - root: /srv/www/pub
            - autoindex: 'on'

      takahe.conf:
        config:
          {%- macro takahe_includes() %}
            - include:
              - snippets/listen
              - snippets/tls_takahe
              - snippets/tls
              - snippets/robots
              - snippets/error
          {%- endmacro %}
          {%- macro takahe_gohome() %}
            - location = /:
                - return: 302 https://social.liberta.casa
          {%- endmacro %}
          {%- set takaheresolver = '- resolver: ' ~ '\'' ~ resolver ~ ' ipv4=off ipv6=on valid=24h\'' -%}
          {#- Main #}
          - server:
            {{ takahe_includes() }}
            - server_name: social.liberta.casa
            - location /:
                - proxy_pass: {{ backend.takahe }}
                {#- does not need X-Real-IP, to-do: add conditional to macro #}
                {{ proxyheaders2() }}
                {{ takaheresolver }}
          {#- Media #}
          - server:
            {{ takahe_includes() }}
            - server_name: social.load.casa
            {{ takahe_gohome() }}
            - location /:
                - proxy_pass: http://media.takahe.rigel.lysergic.dev:8001
                {{ takaheresolver }}
          {#- despair.life is a second entry-point to social.liberta.casa instead of only a secondary domain in Takahe #}
          - server:
            {{ takahe_includes() }}
            - server_name: despair.life
            {{ takahe_gohome() }}
            {#- if someone clicks "Log in" on despair.life, the SAML IDP (Keycloak) would redirect back to despair.life, which breaks the session cookie originating from social.liberta.casa (Django only allows a single "cookie domain" - hence we rewrite the login endpoints to handle sessions exclusively via social.liberta.casa #}
            {%- for talopath in ['auth', 'saml2'] %}
            - location /{{ talopath }}:
                - rewrite: ^/(.*) https://social.liberta.casa/$1 redirect
            {%- endfor %}
            - location /:
                - proxy_pass: {{ backend.takahe }}
                {{ proxyheaders2() }}
                {{ takaheresolver }}
          {#- exhausted.life (secondary domain in Takahe) #}
          - server:
            - include:
              - snippets/listen
              - snippets/tls_exhaustedlife
              - snippets/tls
              - snippets/robots
              - snippets/error
            - server_name: exhausted.life
            {{ takahe_gohome() }}
            - location /.well-known/:
                - proxy_pass: {{ backend.takahe }}
                - sub_filter_types: application/xml
                - sub_filter: takahe.rigel.lysergic.dev:8000 exhausted.life

manage_firewall: True
