include:
  - role.salt.minion
  - .hosts
  - common.{{ grains['os'] | lower }}
