{%- set header = salt['pillar.get']('managed_header_pound') -%}
{%- set sysconfig = '/etc/sysconfig/prometheus-node_exporter' -%}

node_exporter_packages:
  pkg.installed:
    - pkgs:
      - golang-github-prometheus-node_exporter

node_exporter_sysconfig_header:
  file.prepend:
    - name: {{ sysconfig }}
    - text: '{{ header }}'
    - require:
      - pkg: node_exporter_packages

node_exporter_sysconfig:
  file.replace:
    - name: {{ sysconfig }}
    - pattern: |
        ^ARGS=.*$
    - repl: |
        ARGS="--web.listen-address=:9200 --collector.filesystem.fs-types-exclude='^(fuse.s3fs|fuse.cryfs|tmpfscgroup2?|debugfs|devpts|devtmpfs|fusectl|overlay|proc|procfs|pstore)\$' --no-collector.zfs --no-collector.thermal_zone --no-collector.powersupplyclass"
    - require:
      - pkg: node_exporter_packages
      - file: node_exporter_sysconfig_header

node_exporter_service:
  service.running:
    - name: prometheus-node_exporter.service
    - enable: True
    - full_restart: True
    - require:
      - pkg: node_exporter_packages
      - file: node_exporter_sysconfig
    - watch:
      - file: node_exporter_sysconfig
