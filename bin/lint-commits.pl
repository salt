#!/usr/bin/perl
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
use v5.26;

my ($branch_main) = @ARGV;

if(!$branch_main){
        $branch_main = "master"
}

`git ls-remote origin $branch_main` =~ /([a-f0-9]{40})/;

my $refHead = `git rev-parse HEAD`;
my $refTail = $1;

chomp($refHead);
chomp($refTail);

if ($refHead eq $refTail) {
    exit 0;
}

system "gommit check range $refTail $refHead";

if ($? > 0) {
    exit 1;
}
