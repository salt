postfix:
  master_config:
    services:
      smtp:
        enable: False
  config:
    # to-do: set via site config
    relayhost: zz0.email:465
    mydestination: '$myhostname, localhost'
    myhostname: {{ grains['fqdn'] }}
    inet_interfaces: loopback-only
    # to-do: support relay via IPv6
    inet_protocols: ipv4
    alias_database: lmdb:/etc/aliases
    alias_maps: lmdb:/etc/aliases
    smtpd_use_tls: 'no'
    smtp_use_tls: 'yes'
    smtp_tls_security_level: encrypt
    smtp_tls_wrappermode: 'yes'
    disable_dns_lookups: 'yes'

  aliases:
    use_file: false
    present:
      admins: 'georg, pratyush'
      georg: georg@lysergic.dev
      pratyush: pratyush@lysergic.dev
      root: 'system, \root'
      system: system@lysergic.dev
