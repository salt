{%- from slspath ~ '/../map.jinja' import firewall_interfaces, public, internal, backend %}

include:
  - role.salt.common
  - role.salt.minion
  - .mta
  - .ssh

managed_header_pound: |
  ### This file is managed via https://git.com.de/LibertaCasa/salt
  ### Manual changes will be overwritten

{%- if grains['os'] == 'SUSE' %}
zypper:
  refreshdb_force: False

firewalld:
  FlushAllOnReload: 'yes'
  zones:
    internal:
      short: Internal
      {{ firewall_interfaces(internal) }}
      ports:
        - comment: node_exporter
          port: 9200
          protocol: tcp
    public:
      short: Public
      {{ firewall_interfaces(public) }}
    {%- if backend | length %}
    backend:
      {{ firewall_interfaces(backend) }}
    {%- endif %}
{%- endif %}

mine_functions:
  network.ip_addrs: []
  network.ip_addrs6: []
  network.interfaces: []
