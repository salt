{%- set mypillar = salt['pillar.get']('profile:prometheus:targets') %}
{%- set targetsdir = '/etc/prometheus/targets' %}

{%- if mypillar | length %}
{{ targetsdir }}:
  file.directory:
    - group: prometheus

{%- for group, nodes in mypillar.items() %}
{{ targetsdir }}/{{ group }}.json:
  file.serialize:
    - dataset: {{ nodes }}
    - serializer: json
{%- endfor %}

{%- else %}
{%- do salt.log.debug('profile.prometheus: no targets defined') %}
{%- endif %}
