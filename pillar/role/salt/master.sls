{%- import_yaml 'formulas.yaml' as formulas -%}

include:
  - role.salt.common

salt:
  interface: ${'secret_salt:master:interface'}
  master_remove_config: True
  extmods: {'modules': ['nbroles.py'], 'pillar': ['lookup.py']}
  master:
    cache: redis
    cli_summary: True
    # to-do: package extension modules
    extension_modules: /opt/extmods
    gpg_keydir: ${'secret_salt:master:gpg_keydir'}
    ipv6: True
    file_ignore_glob:
      - '*.pyc'
      - '*.swp'
    fileserver_backend:
      - roots
      - git
    file_roots:
      __env__:
        {%- for formula in formulas %}
        - /srv/formulas/{{ formula }}-formula
        {%- endfor %}
    gitfs_root: salt
    gitfs_remotes:
      - https://git.com.de/LibertaCasa/salt.git:
        - user: ${'secret_salt:master:gitfs_remotes:LibertaCasa:user'}
        - password: ${'secret_salt:master:gitfs_remotes:LibertaCasa:password'}
        - fallback: production
    ext_pillar:
      - netbox:
          api_url: ${'secret_salt:master:ext_pillar:netbox:api_url'}
          api_token: ${'secret_salt:master:ext_pillar:netbox:api_token'}
          virtual_machines: True
          interfaces: True
          interface_ips: True
          site_details: False
          site_prefixes: False
      - git:
        - production https://git.com.de/LibertaCasa/salt.git:
          - root: pillar
          - user: ${'secret_salt:master:ext_pillar:git:LibertaCasa:user'}
          - password: ${'secret_salt:master:ext_pillar:git:LibertaCasa:password'}
        - production https://git.com.de/Lysergic/salt.git:
          - root: ${'secret_salt:master:ext_pillar:git:Lysergic:root'}
          - user: ${'secret_salt:master:ext_pillar:git:Lysergic:user'}
          - password: ${'secret_salt:master:ext_pillar:git:Lysergic:password'}
      - lookup: {}
    ext_pillar_first: True
    pillar_merge_lists: True
    pillar_source_merging_strategy: smart
    top_file_merging_strategy: same
    env_order: ['production']
    log_level: info
    show_jid: True
    timeout: 20
    gather_job_timeout: 20
    keep_jobs: 30
    ping_on_rotate: True
    user: ${'secret_salt:master:user'}
    syndic_user: ${'secret_salt:master:syndic_user'}
    cache.redis.unix_socket_path: ${'secret_salt:master:cache.redis.unix_socket_path'}
    cache.redis.password: ${'secret_salt:master:cache.redis.password'}
    rest_cherrypy:
      host: "'::'"
      port: 4550
      ssl_crt: ${'secret_salt:master:rest_cherrypy:ssl_crt'}
      ssl_key: ${'secret_salt:master:rest_cherrypy:ssl_key'}
    auth.ldap.server: ${'secret_salt:master:auth.ldap.server'}
    auth.ldap.port: ${'secret_salt:master:auth.ldap.port'}
    auth.ldap.tls: True
    auth.ldap.scope: 2
    auth.ldap.accountattributename: ${'secret_salt:master:auth.ldap.accountattributename'}
    auth.ldap.groupou: ${'secret_salt:master:auth.ldap.groupou'}
    auth.ldap.groupclass: ${'secret_salt:master:auth.ldap.groupclass'}
    auth.ldap.basedn: ${'secret_salt:master:auth.ldap.basedn'}
    auth.ldap.binddn: ${'secret_salt:master:auth.ldap.binddn'}
    auth.ldap.bindpw: ${'secret_salt:master:auth.ldap.bindpw'}
    auth.ldap.filter: ${'secret_salt:master:auth.ldap.filter'}

firewalld:
  zones:
    internal:
      services:
        - salt-master
      ports:
        - comment: salt-api
          port: 4550
          protocol: tcp
