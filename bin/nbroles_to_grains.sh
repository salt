#!/usr/bin/env sh
# This rewrites top-files to fetch roles from grains instead of our custom roles API. Useful for testing outside of the LibertaCasa infrastructure, but not recommended for production.

potential_files=(*/top.sls salt/common/suse.sls)

for file in ${potential_files[@]}
do
	if [ -f "$file" ]
	then
		files+="$file "
	fi
done

sed -i "s/salt\['http.query'\].*/grains\['roles'\] -%}/" ${files[@]}
