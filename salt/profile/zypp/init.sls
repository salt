zypp_files:
  file.managed:
    - user: root
    - mode: '0644'
    - template: jinja
    - names:
      - /etc/zypp/zypp.conf:
        - source: salt:///{{ slspath }}/files/etc/zypp/zypp.conf.j2
