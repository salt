{%- from slspath ~ '/../map.jinja' import listen_ips, listen_ips4, listen_ips6 -%}

nginx:
  server:
    config:
      daemon: 'off'
      user: []
      events:
        worker_connections: 1024
        use: epoll
      http:
        include:
          - mime.types
          - snippets/modsecurity
          - conf.d/*.conf
          - vhosts.d/*.conf
        tcp_nopush: []
        tcp_nodelay: []
        types_hash_max_size: []
        access_log: []
        error_log: []
        gzip: []
        gzip_disable: []
        log_format main: |-
            '$remote_addr - $remote_user [$time_local] $http_host "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"'
        log_format main-with_ip: |-
            '$remote_addr - $remote_user [$time_local] $http_host "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"'

  snippets:
    robots:
      - location /robots.txt:
        - root: /srv/www/htdocs
    tls:
      - ssl_session_timeout: 1d
      - ssl_session_cache: shared:Lysergic:10m
      - ssl_session_tickets: 'off'
      - ssl_protocols: TLSv1.3
      - ssl_prefer_server_ciphers: 'off'
      - add_header: Strict-Transport-Security "max-age=63072000" always
      - ssl_stapling: 'on'
      - ssl_stapling_verify: 'on'
      - ssl_trusted_certificate: /etc/ssl/ca-bundle.pem
    php-fastcgi:
      - 'location ~* \.php$':
        - fastcgi_index: index.php
        - fastcgi_pass: unix:/run/php-fpm/php-fpm.sock
        - 'include': fastcgi_params
        - fastcgi_param: SCRIPT_FILENAME $document_root$fastcgi_script_name
    {%- if listen_ips | length %}
    listen:
      {%- for ip4 in listen_ips4 %}
      - listen: {{ ip4 }}:443 ssl http2
      {%- endfor %}
      {%- if listen_ips6 | length %}
      {%- for ip6 in listen_ips6 %}
      - listen: '[{{ ip6 }}]:443 ssl http2'
      {%- endfor %}
      {%- endif %}
    {%- endif %}

firewalld:
  zones:
    internal:
      services:
        - http
        - https
